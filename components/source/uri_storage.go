package source

import (
	"bytes"
	"compress/gzip"
	"github.com/juju/errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/cssfs/goscrapper/components/common"
	"gitlab.com/cssfs/goscrapper/models"
	"log"
)

type UriStorage struct {
	httpClient    *fasthttp.Client
	cacheStorage  *CacheStorage
	filename      string
	nameSourceMap models.NameSourceMap
}

func NewUriStorage(httpClient *fasthttp.Client, cacheStorage *CacheStorage, filename string) *UriStorage {
	return &UriStorage{
		httpClient:    httpClient,
		cacheStorage:  cacheStorage,
		filename:      filename,
		nameSourceMap: make(models.NameSourceMap, 32),
	}
}

func (s *UriStorage) Load() error {
	return errors.Trace(common.Load(s.filename, &s.nameSourceMap))
}

func (s *UriStorage) Fetch(name string) (models.SourceSize, error) {
	if result, ok := s.cacheStorage.Get(name); ok {
		return result, nil
	}

	if source, ok := s.nameSourceMap[name]; ok {
		if source.Development == "" || source.Production == "" {
			return models.SourceSize{}, ErrEmptySourceSettings
		}

		developmentContent, err := content(s.httpClient, source.Development)
		if err != nil {
			return models.SourceSize{}, errors.Trace(err)
		}

		developmentSize := len(developmentContent)

		productionContent, err := content(s.httpClient, source.Production)
		if err != nil {
			return models.SourceSize{}, errors.Trace(err)
		}

		gzipSize, err := zipSize(productionContent)
		if err != nil {
			return models.SourceSize{}, errors.Trace(err)
		}

		result := models.SourceSize{
			Development: developmentSize,
			Production:  len(productionContent),
			Gzip:        gzipSize,
		}

		s.cacheStorage.Set(name, result)

		return result, nil
	}

	return models.SourceSize{}, ErrMissingSourceSettings
}

func (s *UriStorage) DefaultAndStore(names []string) error {
	for _, name := range names {
		if _, ok := s.nameSourceMap[name]; !ok {
			s.nameSourceMap[name] = models.Source{}
		}
	}

	return errors.Trace(common.Store(s.filename, s.nameSourceMap))
}

func content(httpClient *fasthttp.Client, uri string) ([]byte, error) {
	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	request.SetRequestURI(uri)

	log.Printf("fasthttp request to %s\n", uri)
	err := httpClient.Do(request, response)
	if err != nil {
		return nil, err
	}

	return response.Body(), nil
}

func zipSize(content []byte) (int, error) {
	buffer := new(bytes.Buffer)
	buffer.Grow(len(content))

	gz := gzip.NewWriter(buffer)
	if _, err := gz.Write(content); err != nil {
		return 0, errors.Trace(err)
	}

	if err := gz.Close(); err != nil {
		return 0, errors.Trace(err)
	}

	return len(buffer.Bytes()), nil
}
