package source

import (
	"github.com/juju/errors"
	"gitlab.com/cssfs/goscrapper/components/common"
	"gitlab.com/cssfs/goscrapper/models"
	"time"
)

type CacheStorage struct {
	filename          string
	now               time.Time
	expirationAfter   time.Time
	nameSourceSizeMap models.SourceSizeEntityMap
	changed           bool
}

func NewCacheStorage(filename string, expiration time.Duration) *CacheStorage {
	now := time.Now()

	return &CacheStorage{
		filename:          filename,
		now:               now,
		expirationAfter:   now.Add(-expiration),
		nameSourceSizeMap: make(models.SourceSizeEntityMap, 32),
		changed:           false,
	}
}

func (s *CacheStorage) Load() error {
	return errors.Trace(common.Load(s.filename, &s.nameSourceSizeMap))
}

func (s *CacheStorage) Get(name string) (models.SourceSize, bool) {
	size, ok := s.nameSourceSizeMap[name]

	if ok && size.Created.After(s.expirationAfter) {
		return models.SourceSize{
			Development: size.Development,
			Production:  size.Production,
			Gzip:        size.Gzip,
		}, true
	}

	return models.SourceSize{}, false
}

func (s *CacheStorage) Set(name string, size models.SourceSize) {
	s.nameSourceSizeMap[name] = models.SourceSizeEntity{
		Development: size.Development,
		Production:  size.Production,
		Gzip:        size.Gzip,
		Created:     s.now,
	}

	s.changed = true
}

func (s *CacheStorage) Store() error {
	if !s.changed {
		return nil
	}

	return errors.Trace(common.Store(s.filename, &s.nameSourceSizeMap))
}
