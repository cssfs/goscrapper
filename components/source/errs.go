package source

import "errors"

var (
	ErrMissingSourceSettings = errors.New("missing source settings")
	ErrEmptySourceSettings   = errors.New("empty source settings")
)
