package common

import (
	"encoding/json"
	"github.com/juju/errors"
	"io/ioutil"
)

func Load(filename string, to json.Unmarshaler) error {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return errors.Trace(err)
	}

	err = to.UnmarshalJSON(content)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func Store(filename string, from json.Marshaler) error {
	content, err := from.MarshalJSON()
	if err != nil {
		return errors.Trace(err)
	}

	err = ioutil.WriteFile(filename, content, 0666)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}
