package dataprovider

import (
	"github.com/juju/errors"
	"gitlab.com/cssfs/goscrapper/components/common"
	"gitlab.com/cssfs/goscrapper/models/dataprovider"
)

type DataProvider struct {
	path string
}

func NewDataProvider(path string) *DataProvider {
	return &DataProvider{path: path}
}

func (p *DataProvider) Load() (dataprovider.Frameworks, error) {
	result := make(dataprovider.Frameworks, 0, 32)

	err := common.Load(p.path, &result)

	if err != nil {
		return nil, errors.Trace(err)
	}

	return result, nil
}

func (p *DataProvider) Store(frameworks dataprovider.Frameworks) error {
	return common.Store(p.path, frameworks)
}
