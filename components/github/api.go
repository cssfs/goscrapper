package github

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/cssfs/goscrapper/models"
	"log"
)

const githubReposPath = "https://api.github.com/repos/"

type Service struct {
	httpClient *fasthttp.Client
	cache      *CacheStorage
}

func NewService(httpClient *fasthttp.Client, cache *CacheStorage) *Service {
	return &Service{httpClient: httpClient, cache: cache}
}

func (s *Service) Stars(name string) (int, error) {
	if result, ok := s.cache.Get(name); ok {
		return result, nil
	}

	request := fasthttp.AcquireRequest()
	response := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseRequest(request)
		fasthttp.ReleaseResponse(response)
	}()

	uri := githubReposPath + name
	request.SetRequestURI(uri)

	log.Printf("fasthttp request to %s\n", uri)
	err := s.httpClient.Do(request, response)
	if err != nil {
		return 0, err
	}

	repoResponse := models.GitHubRepoResponse{}
	err = repoResponse.UnmarshalJSON(response.Body())
	if err != nil {
		return 0, err
	}

	result := repoResponse.StarCount

	s.cache.Set(name, result)

	return result, nil
}
