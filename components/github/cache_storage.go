package github

import (
	"github.com/juju/errors"
	"gitlab.com/cssfs/goscrapper/components/common"
	"gitlab.com/cssfs/goscrapper/models"
	"time"
)

type CacheStorage struct {
	filename        string
	now             time.Time
	expirationAfter time.Time
	nameStarMap     models.GirHubStarEntityMap
	changed         bool
}

func NewCacheStorage(filename string, expiration time.Duration) *CacheStorage {
	now := time.Now()

	return &CacheStorage{
		filename:        filename,
		now:             now,
		expirationAfter: now.Add(-expiration),
		nameStarMap:     make(models.GirHubStarEntityMap, 32),
		changed:         false,
	}
}

func (s *CacheStorage) Load() error {
	return errors.Trace(common.Load(s.filename, &s.nameStarMap))
}

func (s *CacheStorage) Get(name string) (int, bool) {
	star, ok := s.nameStarMap[name]

	if ok && star.Created.After(s.expirationAfter) {
		return star.StarCount, true
	}

	return 0, false
}

func (s *CacheStorage) Set(name string, stars int) {
	s.nameStarMap[name] = models.GitHubStarEntity{
		StarCount: stars,
		Created:   s.now,
	}

	s.changed = true
}

func (s *CacheStorage) Store() error {
	if !s.changed {
		return nil
	}

	return errors.Trace(common.Store(s.filename, &s.nameStarMap))
}
