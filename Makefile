up:
	sudo docker-compose up -d

app:
	sudo docker exec -it cssfs_goscrapper sh

down:
	sudo docker-compose down

easyjson:
	easyjson ./models/github_response.go ./models/github_stars.go ./models/source.go ./models/source_size.go
	easyjson -disallow_unknown_fields ./models/dataprovider/framework.go

run:
	go run main.go

fmt:
	go fmt ./components/... ./models/...
	go fmt main.go

scrap:
	sudo docker-compose up -d
	sudo docker exec cssfs_goscrapper go run main.go
	sudo docker-compose down