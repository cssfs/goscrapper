# GoScrapper

### https://gitlab.com/cssfs/comparator
Source code of comparator must be in same parent as goscrapper
##### For example:
```bash
cd ~/go/src/gitlab.com/cssfs
ls -la
```

```text
4096 сер 17 00:34 .
4096 чер 13 22:16 ..
4096 лип 25 23:46 comparator
4096 сер 18 19:11 goscrapper
```

```bash
make scrap 
```