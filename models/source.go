package models

type Source struct {
	Development string `json:"development"`
	Production  string `json:"production"`
}

// easyjson:json
type NameSourceMap map[string]Source
