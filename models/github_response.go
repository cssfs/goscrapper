package models

// easyjson:json
type GitHubRepoResponse struct {
	StarCount int `json:"stargazers_count"`
}
