package dataprovider

// easyjson:json
type Frameworks []Framework

type Framework struct {
	Name           string        `json:"name"`
	Version        string        `json:"version"`
	License        string        `json:"license"`
	Accessibility  bool          `json:"accessibility"`
	SiteURL        string        `json:"siteUrl"`
	Repository     Repository    `json:"repository"`
	Size           Size          `json:"size"`
	Documentation  string        `json:"documentation"`
	Stackoverflow  Stackoverflow `json:"stackoverflow"`
	Corporation    Corporation   `json:"corporation"`
	Dependencyfree bool          `json:"dependencyfree"`
	Sass           bool          `json:"sass"`
	Less           bool          `json:"less"`
	Adaptability   Adaptability  `json:"adaptability"`
	Requires       []string      `json:"requires"`
	Forum          *Forum        `json:"forum"`
	Gitter         Gitter        `json:"gitter"`
	Authors        []Authors     `json:"authors"`
}

type Repository struct {
	Name  string `json:"name"`
	URL   string `json:"url"`
	Stars int    `json:"stars"`
}

type Size struct {
	Uncompressed int `json:"uncompressed"`
	Minified     int `json:"minified"`
	Gzipped      int `json:"gzipped"`
}

type Stackoverflow struct {
	Tag       string `json:"tag"`
	Questions int    `json:"questions"`
}

type Corporation struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Adaptability struct {
	Desktop bool `json:"desktop"`
	Tablet  bool `json:"tablet"`
	Mobile  bool `json:"mobile"`
}

type Gitter struct {
	URL     string `json:"url"`
	Members int    `json:"members"`
}

type Authors struct {
	Name string `json:"name"`
}

type Forum struct {
	URL     string `json:"url"`
	Members int    `json:"members"`
	Topics  int    `json:"topics"`
}
