package models

import "time"

type SourceSize struct {
	Development int // source code
	Production  int // minified
	Gzip        int // gzip
}

type SourceSizeEntity struct {
	Development int       `json:"development"`
	Production  int       `json:"production"`
	Gzip        int       `json:"gzip"`
	Created     time.Time `json:"created"`
}

// easyjson:json
type SourceSizeEntityMap map[string]SourceSizeEntity
