package models

import "time"

type GitHubStarEntity struct {
	StarCount int       `json:"stargazers_count"`
	Created   time.Time `json:"created"`
}

// easyjson:json
type GirHubStarEntityMap map[string]GitHubStarEntity
