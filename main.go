package main

import (
	"github.com/juju/errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/cssfs/goscrapper/components/dataprovider"
	"gitlab.com/cssfs/goscrapper/components/github"
	"gitlab.com/cssfs/goscrapper/components/source"
	"log"
	"path/filepath"
	"strings"
	"time"
)

const (
	githubURL       = "https://github.com/"
	githubURLLength = len(githubURL)

	frameworkDataProviderPath = "../comparator/public/assets/js/frameworks.json"

	githubCacheStoragePath = "./filestorage/github_cache.json"
	sourceStoragePath      = "./filestorage/source.json"
	sourceCacheStoragePath = "./filestorage/source_size_cache.json"

	githubCacheStorageExpiration = 1 * time.Hour
	sourceCacheStorageExpiration = 1 * time.Hour
)

func main() {
	log.Println("scrapper start")

	frameworkDataProviderPath, err := filepath.Abs(frameworkDataProviderPath)
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	dataProvider := dataprovider.NewDataProvider(frameworkDataProviderPath)
	frameworks, err := dataProvider.Load()
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	githubCacheStoragePath, err := filepath.Abs(githubCacheStoragePath)
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	sourceStoragePath, err := filepath.Abs(sourceStoragePath)
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	sourceCacheStoragePath, err := filepath.Abs(sourceCacheStoragePath)
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	githubCacheStorage := github.NewCacheStorage(githubCacheStoragePath, githubCacheStorageExpiration)
	err = githubCacheStorage.Load()
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}
	defer func() {
		err := githubCacheStorage.Store()

		if err != nil {
			log.Print(errors.Trace(err))
		}
	}()

	sourceSizeCacheStorage := source.NewCacheStorage(sourceCacheStoragePath, sourceCacheStorageExpiration)
	err = sourceSizeCacheStorage.Load()
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}
	defer func() {
		err := sourceSizeCacheStorage.Store()

		if err != nil {
			log.Print(errors.Trace(err))
		}
	}()

	httpClient := new(fasthttp.Client)

	sourceStorage := source.NewUriStorage(httpClient, sourceSizeCacheStorage, sourceStoragePath)
	err = sourceStorage.Load()
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	githubService := github.NewService(httpClient, githubCacheStorage)

	respositories := make([]string, len(frameworks))
	for i := range frameworks {
		repository := frameworks[i].Repository.URL

		if strings.Index(repository, githubURL) != 0 {
			log.Print(errors.Trace(errors.Errorf("expect repository url %s start with %s", repository, githubURL)))

			return
		}

		respositories[i] = repository[githubURLLength:]
	}

	for i := range frameworks {

		repository := respositories[i]

		stars, err := githubService.Stars(repository)
		if err != nil {
			log.Print(errors.Trace(err))

			return
		}

		sourceSize, err := sourceStorage.Fetch(repository)
		if err != nil {
			log.Print(errors.Trace(err))

			if err == source.ErrMissingSourceSettings {
				err := sourceStorage.DefaultAndStore(respositories)

				if err != nil {
					log.Print(errors.Trace(err))
				}
			}

			return
		}

		framework := frameworks[i]

		framework.Repository.Stars = stars
		framework.Size.Uncompressed = sourceSize.Development
		framework.Size.Minified = sourceSize.Production
		framework.Size.Gzipped = sourceSize.Gzip

		frameworks[i] = framework
	}

	err = dataProvider.Store(frameworks)
	if err != nil {
		log.Print(errors.Trace(err))

		return
	}

	log.Println("scrapper complete")
}
